import flask
import json
import os

#from flask import request, jsonify

serverSideTwigs:int = 18
fire:bool = False
food:int = 97
fed:bool = False

def main():

    app.run(debug=True)

app = flask.Flask(__name__,
                  static_url_path='/templates',
                  static_folder='templates',
                  )

@app.route('/')
def index():
    global serverSideTwigs
    return flask.render_template('index.html', twigCount=serverSideTwigs, fire=fire, food=food, fed=fed)


@app.route('/button1/<twigs>', methods=['POST'])
def button1(twigs):
    print(f"twigs: {twigs}")
    #return str(int(twigs)+1)
    global serverSideTwigs
    serverSideTwigs = serverSideTwigs + 1
    return str(serverSideTwigs)

@app.route('/button2/<fireStr>', methods=['POST'])
def button2(fireStr):
    global serverSideTwigs
    global fire
    if serverSideTwigs >=20 and fireStr == "False":
        fireStr="True"
        fire = True
        serverSideTwigs -= 20
#    else : pass
    return {"fireStr":fireStr, "serverSideTwigs":serverSideTwigs}

@app.route('/button3/<harvest>', methods=['POST'])
def button3(harvest):
    global food
    global fed
    food += 1
    if food >= 100:
        fed = True
        food = food - 100
    return {"food":str(food),"fed":fed}

if __name__ == "__main__":
    main()