function button3() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200){
            var response=JSON.parse(this.responseText);
            console.log(response);

            var food = document.getElementById("food");
            food.innerText= response.food;

            var cooking = document.getElementById("cooking");
            cooking.setAttribute("value", response.food);
            var fed = document.getElementById("fed");
            fed.innerText=response.fed;
        }
    };

    var harvest = document.getElementById("harvest").innerHTML;
    xhttp.open("POST", "/button3/" + harvest, true);
    xhttp.send();
}
